# Docker Examples

Examples:
1) [Dockerfile](./dockerfile/)

2) [Hello World from scratch](https://github.com/docker-library/hello-world)

3) [Simple docker-compose.yml](./docker-compose/)

3) [Complex docker-compose.yml](./mysql-phpmyadmin/)
