#!/bin/sh

docker volume create maria-data
docker volume create mysql-data

docker network create maria-and-phpmyadmin
docker network create mysql-and-phpmyadmin

docker run -d \
  --publish 3307:3306 \
  --network maria-and-phpmyadmin \
  --volume maria-data:/var/lib/mysql \
  --env MYSQL_DATABASE=mariaDatabase \
  --env MYSQL_ROOT_PASSWORD=root \
  --env MYSQL_USER=myUser \
  --env MYSQL_PASSWORD=myPassword \
  --name mariadb \
  mariadb:10.8

docker run -d \
  --publish 3308:3306 \
  --network mysql-and-phpmyadmin \
  --volume mysql-data:/var/lib/mysql \
  --env MYSQL_DATABASE=mysqlDatabase \
  --env MYSQL_ROOT_PASSWORD=root \
  --env MYSQL_USER=myUser \
  --env MYSQL_PASSWORD=myPassword \
  --name mysql \
  mysql:8.0

docker run -d \
  --publish 8080:80 \
  --network maria-and-phpmyadmin \
  --network mysql-and-phpmyadmin \
  --env PMA_HOSTS=mysql,mariadb \
  --name phpmyadmin \
  phpmyadmin/phpmyadmin
